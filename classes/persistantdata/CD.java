package persistantdata;

public class CD extends ADocument {

	public CD(int id, String titre, String auteur, Integer emprunteur) {
		super(id, titre, auteur, emprunteur);
	}

	@Override
	public String toString() {
		return "CD " + super.toString();
	}
}
